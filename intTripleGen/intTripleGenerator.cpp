#include <iostream>
#include <fstream>
#include <list>
#include <unordered_map>
#include <cstring>
using namespace std;

class IntegerTripleGenerator {
	
	int totalSubObjCount;
	int totalPredCount;
	int inputTripleCount;

	char *inputFileName;
	int *src,*dest,*edge;

	list <int> srcList;
	list <int> predList;
	list <int> destList;

	unordered_map <string, int> subObjMap;
	unordered_map <string, int> predMap;

	public:
		IntegerTripleGenerator(char *inputFileName);
		bool parseAndCreateGraph();
		void printGraphToFile();
};

IntegerTripleGenerator::IntegerTripleGenerator(char *file) {
	this->inputFileName = file;
	this->totalSubObjCount = 0;
	this->totalPredCount = 0;
	this->inputTripleCount = 0;
}



bool IntegerTripleGenerator::parseAndCreateGraph() {

	ifstream in;
	in.open(inputFileName);
	if(!in.is_open()) {
		cerr << "Unable to open " << inputFileName << endl;
		return false;
	}

	cout << "Reading from " << inputFileName << endl;

	string subject, predicate, object, dot=".";
	while(!in.eof()) {
		getline(in, subject, ' ');

		if(subject == "")
			break;
		getline(in, predicate, ' ');
		if(predicate == "")
			return false;
		getline(in, object);
		if(object == "")
			return false;

		char *tmpObj = new char[object.length() + 1];
		strcpy(tmpObj, object.c_str());
		object = object.substr(0, object.length()-2);

		if(subObjMap.find(subject) == subObjMap.end()) {
			subObjMap[subject] = totalSubObjCount;
			++totalSubObjCount;
		}

		if(subObjMap.find(object) == subObjMap.end()) {
			subObjMap[object] = totalSubObjCount;
			++totalSubObjCount;
		}

		if(predMap.find(predicate) == predMap.end()) {
			predMap[predicate] = totalPredCount;
			++totalPredCount;
		}
		
		int subId = subObjMap[subject];
		int objId = subObjMap[object];
		int predId = predMap[predicate];
		
		// Adding the edge to triple list
		srcList.push_back(subId);
		destList.push_back(objId);
		predList.push_back(predId);
	}
	in.close();

	inputTripleCount = srcList.size();
	src = new int [inputTripleCount];
	dest = new int [inputTripleCount];
	edge = new int [inputTripleCount];
	copy(srcList.begin(), srcList.end(), src);
	copy(destList.begin(), destList.end(), dest);
	copy(predList.begin(), predList.end(), edge);

	cout << "Total triples = " << inputTripleCount << endl;
	cout << "Total vertex count = " << totalSubObjCount << endl;

	// Clearing the lists
	srcList.clear();
	destList.clear();
	predList.clear();

	return true;
}

void IntegerTripleGenerator::printGraphToFile() {
	ofstream out;
	string outputFileName(inputFileName);
	out.open(outputFileName + "_integerTriples");
	
	out <<  totalSubObjCount << " ";
	out <<  inputTripleCount << endl;

	for(uint64_t i=0; i<inputTripleCount; i++)
		out << src[i] << " " << edge[i] << "  " << dest[i] << endl;
	out.close();
	cout << "Created integer triples file " << outputFileName + 
		"_integerTriples" << endl;

	ofstream mapFile;
	mapFile.open("stringLiteral_integer_map");

	mapFile << subObjMap.size() << endl;
	for(unordered_map <string, int>::iterator it = subObjMap.begin();
			it != subObjMap.end(); ++it) {
		mapFile << it->second << " " << it->first << endl;
	}

	mapFile << predMap.size() << endl;
	for(unordered_map <string, int>::iterator it = predMap.begin();
			it != predMap.end(); ++it) {
		mapFile << it->second << " " << it->first << endl;
	}
	mapFile.close();
	cout << "Created literal to integer map file stringLiteral_integer_map" << endl;
}


int main(int argc, char *argv[]) {
	
	IntegerTripleGenerator generator(argv[1]);
	if(!generator.parseAndCreateGraph())
		return 1;	
	generator.printGraphToFile();
	return 0;
}
