RDF3x-MPI
===========
This is an MPI version of the *GH-RDF3x*.

Changes over *GH-RDF3x*:
  - Required compiler GCC47 and OPENMPI-1.4.3 (preferred)
  - Includes two new folders tools/rdf3xvpload and tools/rdf3xvpquery
  - Makefiles have been modified in the corresponding directories to 
    accomodate the new folders

tools/rdf3xvpload:
=================
  - Command usage: bin/rdf3xvpload sample_input.nt
  - This code only accepts RDF graphs which are in .nt (ntriples) format
  - Name stands for rdf3x-vertex-partition-load. Each MPI task is responsible
    for creating its own partition and finally a db-filei.e., the number of 
    partitions is equal to the number of MPI tasks specified. 
  - The ouput will be n partitioned *.nt* files along with their *db* files
  - For debugging purpose, a boolean global varaible runMPI is set to false
    by default. This results in creating a single partition when you run the
    above command.

tools/rdf3xvpquery:
==================
  - Command usage: mpirun -n numb_of_nodes bin/rdf3xvpquery inputFile.nt
  - You should use the same input file name which was used for rdf3xvpload.
  - Running this prompts you for a query. Sample can be found in the file
    'sample_queries' in the base directory, for which the solutions are
    available in 'sample_solutions'
  - Similar to rdf3xvpload, rdf3xvpquery loads each patitioned database in a
    separate MPI task.
  - Task-0 broadcasts the input query and each task runs the query independently

GH-RDF3X
========
*GH-RDF3X* is a more complete and modified version of the original *RDF-3X* engine.
Web site: https://github.com/gh-rdf3x/gh-rdf3x

RDF-3X was created by: Thomas Neumann. Web site: http://www.mpi-inf.mpg.de/~neumann/rdf3x (c) 2008 
RDF-3X was modified by: Hancel Gonzalez and Giuseppe De Simone (Advisor: Maria Esther Vidal). http://github.com/gh-rdf3x/gh-rdf3x (c) 2013
